<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title> The Avengers </title>
	<link rel="icon" href="IMG/ironman.png">
	<link rel="stylesheet" type="text/css" href="CSS/style.css" />
</head>
<body>
<div id="page">
	 <header>
	 	<div>
	 	 <img id="logo" src="IMG/titre.png" />
	 	</div>
	 </header>
	 
     <main>
     
      <section id="intro" >
      <div>        
      	<h1>Bienvenue dans l'univers des Avengers!</h1>         
      	<p>Vous pouvez retrouver sur ce site tous les films Avengers ainsi que les personnages les plus iconics de l'univers Marvel. </p>
      </div>
      </section>
      
      <section id="pages" >
     	<div id = "boutons"> 
			 	 <div><a>Films</a></div>
			 	 <div><a>Personnages</a></div>
			 	 <div><a>Connexion</a></div>
			 	 <audio controls>
			 	 	<source src="MUSIC/avengers.mp3" type="audio/mp3">
			 	 </audio>
			 	 
		</div>
	  </section>
      
     </main>
     
      <footer>
			<div>
	 	 		<img id="marvel" src="IMG/marvel.png" />
	 		</div>
			<div id="noms">
				Concepteurs: Debet Stanislas, Coyla Timoteï, Morales Raphaël & Godet Mélissa
			</div>
			<div>
				<p><a>Nous contacter</a></p>
			</div>
	   </footer>
</div>
</body>
</html>